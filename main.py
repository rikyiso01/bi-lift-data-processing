import csv
import getopt
import sys


def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'hi:', ['help', 'ifile='])
    except getopt.GetoptError:
        print('main.py -i <inputfile>')
        sys.exit(2)
    inputfile = ''
    for opt, arg in opts:
        if opt == '-h':
            print('main.py -i <inputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
    f = open(inputfile)
    f = csv.reader(f, delimiter=',')

    less_cars = 0
    less_motorbike = 0
    would_use = 0
    affected = 0
    target = 0
    total = 0
    first = True

    for row in f:
        if first:
            first = False
            continue
        total += 1
        time = row[0]
        place = row[1]
        age = row[2].split('-')
        uses_bicycle = row[3] == 'Regolarmente'
        transports = row[4].split(', ')
        more_often = row[5] == 'Si'
        obstacles = row[6].split(', ')
        slopes_problems = row[7] != 'No'
        if len(row) < 9:
            roads = row[8]
            will_use_bicycle = row[9] == 'Si'
        else:
            will_use_bicycle = row[8] == 'Si'

        if ('Macchina' in transports) and (
                'Presenza di salite' in obstacles and 'Distanza eccessiva' not in obstacles) and will_use_bicycle:
            less_cars += 1
        if ('Moto' in transports) and (
                'Presenza di salite' in obstacles and 'Distanza eccessiva' not in obstacles) and will_use_bicycle:
            less_motorbike += 1
        if ('Bicicletta' not in transports) and (
                'Presenza di salite' in obstacles and 'Distanza eccessiva' not in obstacles) and will_use_bicycle:
            affected += 1
        if ('Presenza di salite' in obstacles and 'Distanza eccessiva' not in obstacles) \
                and more_often and will_use_bicycle:
            target += 1
        if will_use_bicycle:
            would_use += 1

    print('{:.2f}% would use less often the car'.format(less_cars / total * 100))
    print('{:.2f}% would use less often the moto'.format(less_motorbike / total * 100))
    print('{:.2f}% would use Bi-Lift'.format(would_use / total * 100))
    print('{:.2f}% will use bicycle'.format(affected / total * 100))
    print('{:.2f}% will be targeted'.format(target / total * 100))
    print(total)


if __name__ == '__main__':
    main()
